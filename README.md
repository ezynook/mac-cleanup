<div align="center">
    <img src="https://upload.wikimedia.org/wikipedia/commons/c/cd/Clean-up_no_human-like_stuff.png" width="100">
    <h1>🧹 Mac-Cleanup</h1>
</div>

## 👉 คุณสมบัติ
1. Empty Trash
2. Delete unnecessary logs & files
3. Clear cache
## 👉 ติดตั้ง
✅ ดาวน์โหลดตัวติดตั้ง
```bash
cd ~
curl -o mac-cleanup https://gitlab.com/ezynook/mac-cleanup/-/raw/main/install.sh | bash -s install
```
✅ วิธีการใช้งาน
```bash
$ mac-cleanup
```
> Author: ```Pasit Y.```